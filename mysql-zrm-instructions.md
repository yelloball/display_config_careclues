# INSTALLATION
sudo -i

wget -L https://cdn.zmanda.com/downloads/community/ZRM-MySQL/3.0/Debian/mysql-zrm_3.0.0_all.deb

dpkg -i mysql-zrm_3.0.0_all.deb

apt -f install


# USER SETUP
# I don't know why we do this. check later.
gpasswd -a shibi mysql
# Add the user to 'adm' group to access logs
usermod -aG adm shibi
# Directory where backups will be stored.
mkdir -p /data/backups

chown -R shibi: /data

# BACKUP CONFIGURATION
# Preparing configuration file. Can cp from a sample provided.
cd /etc/mysql-zrm/

mkdir weeklycollection

cp ./mysql-zrm.conf ./weeklycollection/

cd weeklycollection

# Edit the configuration file
vi mysql-zrm.conf

# Edit the following lines; change to the values mentioned
backup-level=0

backup-mode=logical

retention-policy=10D

compress=1 #didn't work for me though, if it doesn't leave it at 0

databases="name-of-our-database"

user="root"

password="******"

verbose=1 #just keep this in 1 until everything works fine

destination=/data/backups

# PERFORM *FULL* BACKUP WITH THIS COMMAND, FROM USER SESSION
sudo mysql-zrm-backup --backup-set weeklycollection

# PERFORM *INCREMENTAL* BACKUP WITH THIS COMMAND, FROM USER SESSION
sudo mysql-zrm-backup --backup-set weeklycollection --backup-level 1

# SCHEDULE FULL & INCREMENTAL BACKUP
# Read this to make it proper, https://wiki.zmanda.com/index.php/Mysql-zrm-scheduler
# I havent tested, but the following might work; this adds to crontab automatically
mysql-zrm-scheduler --add --interval weekly --backup-set weeklycollection --backup-level 0 --day-of-week 6 --start 13:00

mysql-zrm-scheduler --add --interval daily --backup-set weeklycollection --backup-level 1 --start 13:00
