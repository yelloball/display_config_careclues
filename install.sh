# FUNCTIONS
check_exit_status() {
    if [ $? -eq 0 ]
    then
        echo "Success"
    else
        echo "[ERROR] Process Failed!"
        read -p "The last command exited with an error. Exit script? (yes/no) " answer
        if [ "$answer" == "yes" ]
        then
            exit 1
        fi
    fi
}

check_network() {
    echo "########CHECKING NETWORK CONNECTIVITY########"
    if ping -q -c 1 -W 1 google.com >/dev/null; then
        echo "Pass: The network is up"
    else
        echo "Fail: The network is down"
    fi
    echo "#############################################\n\n"
}

get_script_files() {
    echo "########DOWNLOADING SCRIPTS AND FILES########"
    wget -O index.html https://bitbucket.org/yelloball/display_config_careclues/downloads/index.html
    wget -O jquery-3.3.1.min.js https://bitbucket.org/yelloball/display_config_careclues/downloads/jquery-3.3.1.min.js
    wget -O autostart https://bitbucket.org/yelloball/display_config_careclues/downloads/autostart
    wget -O .xscreensaver https://bitbucket.org/yelloball/display_config_careclues/downloads/.xscreensaver
    echo "#############################################\n\n"
}

enable_ssh() {
    echo "########CHECKING SSH SERVICE########"
    check_ssh=`ps -ef | grep ssh | grep -v grep | awk '{print $2}'`
    if [ "${check_ssh}X" != "X" ]
    then
        echo "Pass: SSH is up"
    else
        echo "SSH is down; but hold on"
        sudo systemctl enable ssh
        sudo systemctl start ssh
    fi
    echo "####################################\n\n"
    
}

enable_vnc() {
    echo "########CHECKING VNC SERVICE########"
    check_vnc=`ps -ef | grep vnc | grep -v grep | awk '{print $2}'`
    if [ "${check_vnc}X" != "X" ]
    then
        echo "Pass: VNC is up"
    else
        echo "Fail: Please enable VNC from here..."
        sudo raspi-config
    fi
    echo "####################################\n\n"
}

update() {
    echo "########UPDATING SYSTEM########"
    sudo apt update
    check_exit_status
    sudo apt full-upgrade -y
    check_exit_status
    echo "####################################\n\n"
}

get_display_and_modify_indexhtml() {
    printf "\n"
    read -p "Enter the display number: " url
    sed -i -e "s/xxx/$url/g" index.html
}

install_connectd() {
    sudo apt install connectd
    check_exit_status
}

place_files() {
    echo "########PLACING FILES########"
    mkdir -p /home/pi/.config/lxsession/LXDE-pi
    rm -f /home/pi/.config/lxsession/LXDE-pi/autostart
    rm -f /home/pi/.config/lxsession/LXDE-pi/old.autostart
    rm -f /home/pi/.config/lxsession/LXDE-pi/index.html
    rm -f /home/pi/.config/lxsession/LXDE-pi/jquery-3.3.1.min.js
    rm -f /home/pi/.xscreensaver
    
    mv ./index.html /home/pi/.config/lxsession/LXDE-pi/
    mv ./jquery-3.3.1.min.js /home/pi/.config/lxsession/LXDE-pi/
    mv ./autostart /home/pi/.config/lxsession/LXDE-pi/
    mv ./.xscreensaver /home/pi/.xscreensaver
    echo "####################################\n\n"
}

install_configure_xscreensaver() {
    sudo apt install xscreensaver
    sed -i -e "s/mode:.*/mode: off/g" /home/pi/.xscreensaver
}
####################################################################

printf "\n"
PS3='Please enter your choice: '
options=("Full setup" "Change display number" "Reboot to normal mode to configure WiFi, etc." "Reboot to kiosk mode" "Shutdown" "Reboot" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Full setup")
            # Check network connection
            check_network
            # wget necessary scripts and files
            # - html
            # - js
            get_script_files
            # Enable ssh
            enable_ssh
            # Enable vnc
            enable_vnc
            # Update system
            update
            # Install connectd
            install_connectd
            # Configure connectd
            sudo connectd_installer
            # Install xscreensaver to disable screensaver blanking out
            install_configure_xscreensaver
            # Get display number
            get_display_and_modify_indexhtml
            # Place files
            # - html
            # - js
            place_files
            echo "ALL DONE!! REBOOT NOW"
        ;;
        "Change display number")
            if [ ! -f /home/pi/.config/lxsession/LXDE-pi/autostart ]; then
                echo "It looks like you have not setup the device; exit and do a full setup"
                printf "\n"
            else
                printf "\n"
                read -p "Enter display number: " displayno
                # remove old file
                rm -f /home/pi/.config/lxsession/LXDE-pi/index.html
                # get a fresh file
                wget -O index.html https://bitbucket.org/yelloball/display_config_careclues/downloads/index.html
                # change display number
                sed -i -e "s/xxx/$displayno/g" index.html
                # place the file
                mv ./index.html /home/pi/.config/lxsession/LXDE-pi/
                printf "Changed the display number. Rebooting...\n"
                reboot
            fi
        ;;
        "Reboot to normal mode to configure WiFi, etc.")
            if [ ! -f /home/pi/.config/lxsession/LXDE-pi/autostart ]; then
                echo "You are already in a normal mode!"
                printf "\n"
            else
                mv /home/pi/.config/lxsession/LXDE-pi/autostart /home/pi/.config/lxsession/LXDE-pi/old.autostart
                printf "\n"
                echo "Changed mode to normal. Rebooting..."
                reboot
            fi
        ;;
        "Reboot to kiosk mode")
            if [ ! -f /home/pi/.config/lxsession/LXDE-pi/old.autostart ]; then
                echo "It is already in kiosk mode; check the url please."
                printf "\n"
            else
                mv /home/pi/.config/lxsession/LXDE-pi/old.autostart /home/pi/.config/lxsession/LXDE-pi/autostart
                echo "Changed to kiosk mode. Rebooting..."
                printf "\n"
                reboot
            fi
        ;;
        "Shutdown")
            shutdown now
        ;;
        "Reboot")
            reboot
        ;;
        "Quit")
            break
        ;;
        *) echo "invalid option $REPLY";;
    esac
done
