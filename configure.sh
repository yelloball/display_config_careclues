#!/bin/bash
# Bash Menu Script
printf "\n"
PS3='Please enter your choice: '
options=("Initialize or change URL" "Reboot to normal mode to configure WiFi, etc." "Reboot to kiosk mode" "Shutdown" "Reboot" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Initialize or change URL")
            printf "\n"
            read -p "Enter URL: " url
            mkdir -p /home/pi/.config/lxsession/LXDE-pi
            
            rm -f /home/pi/.config/lxsession/LXDE-pi/autostart
            rm -f /home/pi/.config/lxsession/LXDE-pi/old.autostart

            echo @xset s off >> /home/pi/.config/lxsession/LXDE-pi/autostart
            echo @xset -dpms >> /home/pi/.config/lxsession/LXDE-pi/autostart
            echo @xset s noblank >> /home/pi/.config/lxsession/LXDE-pi/autostart
            echo @chromium-browser --kiosk $url >> /home/pi/.config/lxsession/LXDE-pi/autostart
            printf "Changed the URL. Rebooting...\n"
            reboot
            ;;
        "Reboot to normal mode to configure WiFi, etc.")
            if [ ! -f /home/pi/.config/lxsession/LXDE-pi/autostart ]; then
                echo "You are already in a normal mode!"
                printf "\n"
            else 
                mv /home/pi/.config/lxsession/LXDE-pi/autostart /home/pi/.config/lxsession/LXDE-pi/old.autostart
                printf "\n"
                echo "Changed mode to normal. Rebooting..."
                reboot
            fi
            ;;
        "Reboot to kiosk mode")
            if [ ! -f /home/pi/.config/lxsession/LXDE-pi/old.autostart ]; then
                echo "It is already in kiosk mode; check the url please."
                printf "\n"
            else 
                mv /home/pi/.config/lxsession/LXDE-pi/old.autostart /home/pi/.config/lxsession/LXDE-pi/autostart
                echo "Changed to kiosk mode. Rebooting..."
                printf "\n"
                reboot
            fi
            ;;
	"Shutdown")
	    shutdown now
	    ;;
	"Reboot")
	    reboot
	    ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
